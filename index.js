const characters = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9","~","`","!","@","#","$","%","^","&","*","(",")","_","-","+","=","{","[","}","]",",","|",":",";","<",">",".","?",
"/"];

// Grab password divs to display values later on.
const password1Div = document.getElementById("password1");
const password2Div = document.getElementById("password2");

const charLimit = 15;

// The function takes no arguments and generates the random password.
// Returns the generated `currentPassword` value.
function generatePassword() {
    
    // Define & reset currentPassword to avoid leftover passwords from previous runs.
    let currentPassword = "";
    let randomNumber;

    // Runs the loop until it hits the character limit, and generates random numbers:
    for ( let i = 0; i < charLimit; i++ ) {
        randomNumber = Math.floor(Math.random() * characters.length);
        currentPassword += characters[randomNumber];
    }

    return currentPassword;

   
}

// When the `passw-gen` button clicked, run this function to grab 2 randomly generated passwords, 
// and display them in their respective divs.
function grabRandomPasswords() {
    password1Div.textContent = generatePassword();
    password2Div.textContent = generatePassword();
}